package com.abdullahsen.assignment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.abdullahsen.assignment.databinding.ActivityMainBinding
import com.abdullahsen.assignment.ui.capture.BottomDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.FirebaseApp
import dagger.hilt.android.AndroidEntryPoint

private lateinit var binding: ActivityMainBinding
private val numberOfTabs = 2

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), BottomDialog.OnSelectLibrary {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        FirebaseApp.initializeApp(this)

        binding.tabLayout.tabMode = TabLayout.MODE_FIXED

        // Set the ViewPager Adapter
        val adapter = TabsPagerAdapter(supportFragmentManager, lifecycle, numberOfTabs)
        binding.viewPager.adapter = adapter

        // Disable Swipe
        binding.viewPager.isUserInputEnabled = false

        // Link the TabLayout and the ViewPager2 together and Set Text & Icons
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            when (position) {
                0 -> {
                    tab.text = "CAPTURE"
                }
                1 -> {
                    tab.text = "LIBRARY"
                }
            }
        }.attach()

        binding.viewPager.setCurrentItem(1,false)

    }

    override fun OnLibrarySelected() {
        binding.viewPager.setCurrentItem(1,false)
    }
}