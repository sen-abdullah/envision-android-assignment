package com.abdullahsen.assignment.ui.util

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.DateFormat
import java.text.SimpleDateFormat

/**
 * BindingAdapters.kt file has one binding method.
 * It is responsible for formatting TimeMillis to yyyy/MM/dd HH:mm:ss .
 */
@BindingAdapter("formattedDate")
fun bindMillisToFormattedDateAndTime(textView: TextView, millis: Long) {
    System.currentTimeMillis()
    val dateFormat: DateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
    val formattedDate = dateFormat.format(millis)
    textView.text = formattedDate
}
