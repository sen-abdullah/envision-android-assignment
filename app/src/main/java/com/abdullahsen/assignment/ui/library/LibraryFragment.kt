package com.abdullahsen.assignment.ui.library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.abdullahsen.assignment.databinding.FragmentLibraryBinding
import com.abdullahsen.assignment.ui.library.adapter.TextAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LibraryFragment : Fragment() {

    private lateinit var binding: FragmentLibraryBinding
    private lateinit var adapter: TextAdapter
    private val libraryViewModel: LibraryViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentLibraryBinding.inflate(inflater);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        libraryViewModel.texts.observe(viewLifecycleOwner, {
            it?.let {
                adapter = TextAdapter()
                adapter.submitList(it.toMutableList())
                binding.recyclerViewPropertyList.adapter = adapter
            }
        })

    }
}