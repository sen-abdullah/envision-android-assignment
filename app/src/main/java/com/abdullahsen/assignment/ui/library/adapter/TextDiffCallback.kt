package com.abdullahsen.assignment.ui.library.adapter

import androidx.recyclerview.widget.DiffUtil
import com.abdullahsen.assignment.database.model.Text

//to determine which items have changed
class TextDiffCallback : DiffUtil.ItemCallback<Text>() {
    override fun areItemsTheSame(oldItem: Text, newItem: Text): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Text, newItem: Text): Boolean {
        return oldItem.id == newItem.id
    }
}