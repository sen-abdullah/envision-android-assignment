package com.abdullahsen.assignment.ui.library

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.abdullahsen.assignment.repository.TextRepository

/**
 * LibraryViewModel class is designed to
 * fetch texts.
 */
class LibraryViewModel @ViewModelInject constructor(textRepository: TextRepository): ViewModel() {

    //fetches texts from our database
    val texts = textRepository.texts

}
