package com.abdullahsen.assignment.ui.capture

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.util.Rational
import android.util.Size
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.exifinterface.media.ExifInterface
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.abdullahsen.assignment.R
import com.abdullahsen.assignment.database.model.Text
import com.abdullahsen.assignment.databinding.FragmentCaptureBinding
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_capture.*
import java.io.File

private const val REQUEST_CODE_PERMISSIONS = 10
private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)

@AndroidEntryPoint
class CaptureFragment : Fragment() {

    private val captureViewModel: CaptureViewModel by viewModels()
    private var bottomDialog: BottomDialog = BottomDialog()

    private var isImageCaptured = false
    private var isTextSaved = false
    private lateinit var imageCapture: ImageCapture
    private lateinit var preview: Preview
    private lateinit var binding: FragmentCaptureBinding
    private lateinit var text:String


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentCaptureBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (allPermissionsGranted()) {
            binding.viewFinder.post {  startCamera() }
        } else {
            ActivityCompat.requestPermissions(requireActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
        binding.viewFinder.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }

    override fun setMenuVisibility(menuVisible: Boolean) {
        super.setMenuVisibility(menuVisible)
        if(menuVisible){
            binding.buttonCaptureOrSave.isEnabled = true
            isImageCaptured = false
            resetCamera()
        }
    }

    private fun startCamera() {
        initializePreview()
        initializeCameraConfig()
        bindCameraX()
        binding.buttonCaptureOrSave.setOnClickListener{
            if(isImageCaptured && !isTextSaved ){
                val textObject = Text(text = text)
                captureViewModel.insertText(textObject)
                binding.buttonCaptureOrSave.isEnabled = false
                bottomDialog.show(childFragmentManager,"TAG")
            } else {
                binding.textViewProgressInfo.visibility = View.VISIBLE
                binding.buttonCaptureOrSave.visibility = View.INVISIBLE
                takePicture()
                isImageCaptured = true
            }
        }
    }

    private fun resetCamera() {
        CameraX.unbindAll()
        binding.viewFinder.visibility = View.VISIBLE
        binding.textViewAnalysedText.visibility = View.GONE
        binding.textViewProgressInfo.visibility = View.GONE
        binding.buttonCaptureOrSave.visibility = View.VISIBLE
        binding.buttonCaptureOrSave.text = getString(R.string.capture)
        binding.textViewAnalysedText.setTextIsSelectable(false)
        isImageCaptured = false
        startCamera()
    }

    private fun initializePreview() {
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetAspectRatio(Rational(1, 1))
            setTargetResolution(Size(640, 640))
        }.build()

        preview = Preview(previewConfig)
        preview.setOnPreviewOutputUpdateListener {
            val parent =  binding.viewFinder.parent as ViewGroup
            parent.removeView( binding.viewFinder)
            parent.addView( binding.viewFinder, 0)
            binding.viewFinder.setSurfaceTexture(it.surfaceTexture)
            updateTransform()
        }
    }

    private fun initializeCameraConfig() {
        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                setTargetAspectRatio(Rational(1, 1))
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            }.build()
        imageCapture = ImageCapture(imageCaptureConfig)
    }

    private fun bindCameraX(){
        CameraX.bindToLifecycle(this, preview, imageCapture)
    }

    private fun takePicture() {
        val file = File(requireActivity().externalMediaDirs.first(), "${System.currentTimeMillis()}.jpg")
        imageCapture.takePicture(file, object : ImageCapture.OnImageSavedListener {
            override fun onError(error: ImageCapture.UseCaseError, message: String, exc: Throwable?) {
                val msg = "Photo capture failed: $message"
                Log.e("Assignment App", msg)
                exc?.printStackTrace()
            }
            override fun onImageSaved(file: File) {
                val msg = "Photo capture succeeded: ${file.absolutePath}"
                showImageOnView(file.path)
                file.delete()
            }
        })
    }

    private fun showImageOnView(path : String) {
        val uri = Uri.fromFile(File(path))
        val exifInterface = ExifInterface(uri.path!!)
        val orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, uri)
        val rotatedBitmap = rotateBitmap(bitmap, orientation)
        processImage(rotatedBitmap)
    }

    private fun processImage(bitmap : Bitmap?) {
        bitmap?.let {
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val detector = FirebaseVision.getInstance().onDeviceTextRecognizer
            detector.processImage(image).addOnSuccessListener { firebaseVisionText ->
                val analysedText = firebaseVisionText.text
                if(!analysedText.isNullOrBlank()) {
                    text = analysedText
                    binding.buttonCaptureOrSave.visibility = View.VISIBLE
                    binding.buttonCaptureOrSave.text =getString(R.string.save_text_to_library)
                    binding.textViewAnalysedText.text = analysedText
                    binding.textViewAnalysedText.setTextIsSelectable(true)
                    binding.viewFinder.visibility = View.INVISIBLE
                    binding.textViewAnalysedText.visibility = View.VISIBLE
                    binding.textViewProgressInfo.visibility = View.INVISIBLE
                    CameraX.unbindAll()
                } else {
                    resetCamera()
                    Toast.makeText(requireActivity(), getString(R.string.no_text_is_found), Toast.LENGTH_SHORT).show()
                }
            }
                .addOnFailureListener {
                    Toast.makeText(requireActivity(), getString(R.string.no_text_is_found), Toast.LENGTH_SHORT).show()
                    it.printStackTrace()
                    resetCamera()
                }
        }
    }

    private fun rotateBitmap(bitmap: Bitmap, orientation: Int): Bitmap? {
        val matrix = Matrix()
        when (orientation) {
            ExifInterface.ORIENTATION_NORMAL -> return bitmap
            ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.setScale(-1f, 1f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.setRotate(180f)
            ExifInterface.ORIENTATION_FLIP_VERTICAL -> {
                matrix.setRotate(180f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_TRANSPOSE -> {
                matrix.setRotate(90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.setRotate(90f)
            ExifInterface.ORIENTATION_TRANSVERSE -> {
                matrix.setRotate(-90f)
                matrix.postScale(-1f, 1f)
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.setRotate(-90f)
            else -> return bitmap
        }
        try {
            val bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            bitmap.recycle()
            return bmRotated
        } catch (e: OutOfMemoryError) {
            Toast.makeText(requireActivity(), getString(R.string.unable_to_load_image), Toast.LENGTH_SHORT).show()
            e.printStackTrace()
            return null
        }
    }

    private fun updateTransform() {
        val matrix = Matrix()
        val centerX = view_finder.width / 2f
        val centerY = view_finder.height / 2f
        val rotationDegrees = when (binding.viewFinder.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)
        binding.viewFinder.setTransform(matrix)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                binding.viewFinder.post { startCamera() }
            } else {
                Toast.makeText(requireActivity(), getString(R.string.permission_is_not_granted), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun allPermissionsGranted(): Boolean {
        for (permission in REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(requireActivity(), permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }
}




