package com.abdullahsen.assignment.ui.capture

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.abdullahsen.assignment.database.model.Text
import com.abdullahsen.assignment.repository.TextRepository
import kotlinx.coroutines.launch

/**
 * CaptureViewModel class is designed to
 * insert text related data proper way.
 */
class CaptureViewModel @ViewModelInject constructor
    (private val textRepository: TextRepository) : ViewModel() {

    //adds text to database
    fun insertText(text:Text){
        viewModelScope.launch {
            textRepository.insertText(text)
        }
    }
}
