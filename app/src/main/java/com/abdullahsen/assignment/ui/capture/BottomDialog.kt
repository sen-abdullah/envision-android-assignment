package com.abdullahsen.assignment.ui.capture

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.abdullahsen.assignment.databinding.BottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class BottomDialog  : BottomSheetDialogFragment() {

    private lateinit var listener: OnSelectLibrary
    private lateinit var binding: BottomSheetBinding

    interface OnSelectLibrary {
        fun OnLibrarySelected()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnSelectLibrary) {
            listener = context
        } else {
            throw ClassCastException(
                "$context must implement OnSelectLibrary."
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = BottomSheetBinding.inflate(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.textViewShowLibrary.setOnClickListener {
            listener.OnLibrarySelected()
            dismiss()
        }

    }
}