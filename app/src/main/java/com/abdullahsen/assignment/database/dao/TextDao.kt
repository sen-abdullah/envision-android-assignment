package com.abdullahsen.assignment.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.abdullahsen.assignment.database.model.Text

@Dao
interface TextDao {

    @Query("select * from Text")
    fun getTexts(): LiveData<List<Text>>

    @Query("SELECT * from Text WHERE id = :key")
    fun getPropertyById(key: Long): LiveData<Text>

    @Insert
    suspend fun insertText(text: Text)

}