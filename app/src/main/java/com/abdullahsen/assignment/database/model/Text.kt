package com.abdullahsen.assignment.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Text (@PrimaryKey(autoGenerate = true) var id: Long = 0L,
                 val time : Long = System.currentTimeMillis(),
                 val text : String)