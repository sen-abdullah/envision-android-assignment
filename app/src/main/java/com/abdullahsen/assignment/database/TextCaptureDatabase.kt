package com.abdullahsen.assignment.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.abdullahsen.assignment.database.dao.TextDao
import com.abdullahsen.assignment.database.model.Text

@Database(entities = [Text::class], version = 1)
abstract class TextCaptureDatabase : RoomDatabase() {

    abstract fun textDao(): TextDao

}