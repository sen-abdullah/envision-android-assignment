package com.abdullahsen.assignment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.abdullahsen.assignment.ui.capture.CaptureFragment
import com.abdullahsen.assignment.ui.library.LibraryFragment

class TabsPagerAdapter (fm: FragmentManager,
                        lifecycle: Lifecycle,
                        private var numberOfTabs: Int) : FragmentStateAdapter(fm, lifecycle) {

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                // # CaptureFragment
                return CaptureFragment()
            }
            1 -> {
                // # LibraryFragment
                return LibraryFragment()
            }
            else -> return LibraryFragment()
        }
    }

    override fun getItemCount(): Int {
        return numberOfTabs
    }

}