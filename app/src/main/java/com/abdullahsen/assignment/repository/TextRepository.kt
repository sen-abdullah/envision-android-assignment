package com.abdullahsen.assignment.repository

import androidx.lifecycle.LiveData
import com.abdullahsen.assignment.database.TextCaptureDatabase
import com.abdullahsen.assignment.database.model.Text
import javax.inject.Inject

/**
 * TextRepository handles data operations.
 * It is a layer between viewmodels and localdatabase.
 * Changes in database trigger callbacks on active LiveData.
 * Database serves as the single source of truth.
 */
class TextRepository @Inject constructor(private val database: TextCaptureDatabase) {

    //gets all texts from database
    val texts: LiveData<List<Text>> = database.textDao().getTexts()


    //inserts detected text
    suspend fun insertText(text: Text) {
        database.textDao().insertText(text)
    }

}